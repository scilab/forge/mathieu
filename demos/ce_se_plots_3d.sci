// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

// Comparison of ce and se plots with respect to those presented on Fig. 2, p. 235 of the article
// J. C. Gutiérrez-Vega, R. M. Rodríguez-Dagnino, M. A. Meneses-Nava, and S. Chávez-Cerda, "Mathieu functions, a visual approach",
// American Journal of Physics, 71 (233), 233-242.
// [DOI: 10.1119/1.1522698]
//

num_points=100;
x=linspace(0,%pi,num_points);
y=ones(1,num_points);
z=zeros(length(x));


kinds = ['ce' 'ce' 'ce'; 'se' 'se' 'se'];
orders = [0 1 2; 1 2 3];
q_min = 0;
q_step = 2;
q_max = 30;

[n_max m_max] = size(orders);


  s_wh = get(0, 'screensize_px');
  margin_h = 50;
  margin_w = 5;
  max_w = s_wh(3)-2*margin_w;
  max_h = s_wh(4)-2*margin_h;     
  h = scf();
  h.figure_name = 'Comparison of ce and se graphs with presented on Fig. 2, p. 235 of the article J. C. Gutiérrez-Vega, et al. Mathieu functions, a visual approach';

p=1;
for n=1:n_max
  for m=1:m_max
    subplot(n_max,m_max,p);
    
    for q=q_min:q_step:q_max
      z=mathieu_mathieu(kinds(n,m), orders(n,m), x, q);
        plot3d3(x, q*y, z, theta=-93.6, alpha=87.2);
        mtlb_hold('on');
    end
    name_tex='$'+sprintf('%s', kinds(n, m))+'_'+sprintf('%d', orders(n, m))+'(\eta; q)$';
    xtitle(name_tex,'$\eta$','$q$','');
      
      h=gca();
      h.tight_limits='on';
      h.box='off';
      h.x_ticks = tlist(['ticks','locations','labels'],[0,%pi/2,%pi],['0','$\pi/2$','$\pi$']);
      h.y_ticks = tlist(['ticks','locations','labels'],[0,30],['0','30']);
      h.z_ticks = tlist(['ticks','locations','labels'],[-2.0,-1.5,-1.,-0.5,0,0.5,1,1.5,2],['-2.0','-1.5','-1.0','-0.5','0','0.5','1.0','1.5','2.0']);
      h.sub_ticks = [0,14,4];
      h.title.font_size=3;
      h.title.font_style=4;
      h.x_label.font_size=4;
      h.y_label.font_size=4;
      h.z_label.font_size=4;      
      h.font_size=3;      
      
      xgrid
      
    p = p+1;
  end
end

  h=gcf();  
  h.figure_position = [margin_w margin_h];
  h.figure_size = [max_w max_h];
