// Copyright (C) 2016 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function [X, Y, ZETA, is_even_or_odd, f, qs, ws, ka] = mathieu_membrane_mode(a, b, m, n, func_name, bc_type, N_xi, N_eta, mode_options)
// MATHIEU_MEMBRANE_MODE Calculate elliptical membrane mode for known semi-axes, mode numbers, mode type and boundary condition (Soft/Dirichlet or Hard/Neumann).
//
//   Arguments:
//      a - membrane semi-major axis (along x)
//      b - membrane semi-minor axis (along y)
//      m - angular variations (function order)
//      n - radial variations (number of q roots)
//      func_name - radial function name for mode (should be one of 'Mc1', 'Ms1', 'Ce', 'Se')
//      bc_type - boundary condition type (%t - Dirichlet, %f - Neumann)
//      N_xi - grid size on xi (ellipses) direction
//      N_eta - grid size on eta (hyperbolas) direction
//      mode_options - optional structure with options
//          .q_start - initial mathieu_rootfinder guess for 'q' (default 1e-3)
//          .q_delta - mathieu_rootfinder step increment on search of 'q' (default (a/b)*n*abs(1 - xi0)*(m+1) )
//          .q_fun_tol - relative mathieu_rootfinder tolerance of zero value (fsolve function tolerance, default 1e-12)
//          .q_maxiter - maximum number of mathieu_rootfinder iterations (default 1e4)
//          .do_print - print mathieu_rootfinder infromation in console? (boolean)
//          .do_plot_root - plot mathieu_rootfinder graphics of rootfinding? (boolean)
//          .do_plot_mode - plot membrane mode? (boolean, default %t)
//          .font_size_axes - font size for the axes (default 3)
//          .show_colorbar - show colorbar? (boolean, default %t)
//
//  Return values:
//      X - N_xi*N_eta matrix with Cartesian x coordinate of mode surface
//      Y - N_xi*N_eta matrix with Cartesian y coordinate of mode surface
//      is_even_or_odd - mode type (boolean: %t - even, %f - odd)
//      f - ellipse foci
//      qs - array of found q values
//      ws - characteristic frequencies ws = 2*sqrt(qs) / f as in [2]
//      ka - dimensionless frequency parameter ka = 2*sqrt(qs) / sqrt(1-(b/a)^2) as in [1]
//
// References:
// 1. Wilson, Howard B., and Robert W. Scharstein. "Computing elliptic membrane high frequencies by Mathieu and Galerkin methods." Journal of Engineering Mathematics 57.1 (2007): 41-55. (online at http://scharstein.eng.ua.edu/ENGI1589.pdf or http://dx.doi.org/10.1007/s10665-006-9070-1 )
// 2. Gutiérrez-Vega, J., S. Chávez-Cerda, and Ramón Rodríguez-Dagnino. "Free oscillations in an elliptic membrane." Revista Mexicana de Fisica 45.6 (1999): 613-622. (online at http://optica.mty.itesm.mx/pmog/Papers/P001.pdf )
//
    // local function
    function [zeta, is_even_or_odd]=ell_mode(xi, eta, m, n, q, func_name)
        is_even_or_odd = %t;
        
        select func_name
            // even
            case 'Mc1' then
                rad = mathieu_rad_mc(m, q(n), xi, 1, 1);
                is_even_or_odd = %t;
    
            case 'Ce' then
                rad = mathieu_rad_ce(m, q(n), xi, 1);
                is_even_or_odd = %t;
    
            // odd
            case 'Ms1' then
                rad = mathieu_rad_ms(m, q(n), xi, 1, 1);
                is_even_or_odd = %f;
            
            case 'Se' then
                rad = mathieu_rad_se(m, q(n), xi, 1);
                is_even_or_odd = %f;
        end
        
        if is_even_or_odd then
            ang = mathieu_ang_ce(m, q(n), eta,1);
        else
            ang = mathieu_ang_se(m, q(n), eta,1);
        end
        
        zeta = ang .* rad;
    endfunction
    //

    // rootfinding
        // prepare
        f = sqrt(a^2 - b^2);
        ecc = f/a;
        [xi0, eta_tmp] = mathieu_cart2ell(a, 0, f);

        // total roots to be found
        q_roots_tot = ceil(n);

        // set rootfinder parameters
        do_plot_root = %f;
        do_print = %f;
        do_plot_mode = %t;

            q_start = 1e-3;
            q_delta = (a/b)*n*abs(1 - xi0)*(m+1);
            
            q_fun_tol = 1e-12;
            q_maxiter = 1e4;

        // set font size
        font_size_axes = 3;

        // colorbar
        show_colorbar = %t;

        if argn(2) == 9 then
            // q_start
            if isfield(mode_options,'q_start') then
                q_start = mode_options.q_start;
            end
            // q_delta
            if isfield(mode_options,'q_delta') then
                q_delta = mode_options.q_delta;
            end
            // q_fun_tol
            if isfield(mode_options,'q_fun_tol') then
                q_fun_tol = mode_options.q_fun_tol;
            end
            // q_maxiter
            if isfield(mode_options,'q_maxiter') then
                q_maxiter = mode_options.q_maxiter;
            end

            // do_print
            if isfield(mode_options,'do_print') then
                do_print = mode_options.do_print;
            end
            // do_plot
            if isfield(mode_options,'do_plot_root') then
                do_plot_root = mode_options.do_plot_root;
            end
            // do_plot_mode
            if isfield(mode_options,'do_plot_mode') then
                do_plot_mode = mode_options.do_plot_mode;
            end
            // axes font size
            if isfield(mode_options,'font_size_axes') then
                font_size_axes = mode_options.font_size_axes;
            end
            // colorbar
            if isfield(mode_options,'show_colorbar') then
                show_colorbar = mode_options.show_colorbar;
            end
        end

    // set axes, clear and hide plot
    if do_plot_mode then
        ax = gca();
        if ~isempty(ax.children) then
            delete(ax.children);
        end
        set(ax,"Visible","off");
    end

        // call rootfinder
        qs = mathieu_rootfinder(m, xi0, q_start, q_delta, q_roots_tot, q_fun_tol, q_maxiter, func_name, bc_type, do_print, do_plot_root)';

        // plot prepare
        xi  = linspace(0, xi0, N_xi);
        eta = linspace(0, 2*%pi, N_eta);

        [XI, ETA] = ndgrid(xi, eta);

        XI_col = matrix(XI, 1, N_xi*N_eta);
        ETA_col = matrix(ETA, 1, N_xi*N_eta);
        [ZETA_col, is_even_or_odd] = ell_mode(XI_col, ETA_col, m, n, qs, func_name);
        ZETA = matrix(ZETA_col, N_xi, N_eta);

        X = zeros(N_xi, N_eta);
        Y = zeros(N_xi, N_eta);

        for i=1:N_xi
            for j=1:N_eta
                [X(i,j), Y(i,j)] = mathieu_ell2cart(xi(i), eta(j), f);
            end
        end

        if ~isempty(qs) then
            // calculate and show eigenfrequiences
                ws = 2*sqrt(qs) / f;
                ka = 2*sqrt(qs) / sqrt(1-(b/a)^2);

            // plot membrane mode
            if do_plot_mode then
                // plot prepare
                if is_even_or_odd then
                    ell_mode_str = 'Even';
                else
                    ell_mode_str = 'Odd';
                end
                if bc_type then
                    bc_type_txt = "Soft";
                else
                    bc_type_txt = "Hard";
                end

            // show axes and plot
                set(ax,"Visible","on");

                gcf().color_map = jetcolormap(128);
                    ZETA = real(ZETA);
                    ZETA = (ZETA - min(ZETA))/(max(ZETA)-min(ZETA));
                    surf(X,Y,ZETA,"facecol","interp");
                    e = gce();
                    e.color_mode = -1;

                    ax.isoview = 'on';
//                    ax.rotation_angles=[0,-90];
                    replot([-a,-a,a,a]);
                    ax.tight_limits = 'on';

                    // title and labels
                    title('$\text{'+ell_mode_str+' '+bc_type_txt+' mode with }m='+string(m)+',\ n='+string(n)+'$');
                    xlabel('$x,\ \text{m}$');
                    ylabel('$y,\ \text{m}$');
                    zlabel('$\text{mode}$');
        
                    ax.title.font_size = font_size_axes+1;
                    ax.font_size = font_size_axes-1;
                    ax.x_label.font_size = font_size_axes; 
                    ax.y_label.font_size = font_size_axes;
                    ax.z_label.font_size = font_size_axes;
                    ax.margins = [0.1 0.15 0.1 0.1];
                    xgrid;
        
                // colorbar
                if show_colorbar then
                    Zmin = min(real(ZETA));
                    Zmax = max(real(ZETA));
                    colorbar(Zmin, Zmax);
                end

                // print
                if do_print then
                    // for comparison with [1]
                    disp('ka as in Tables [Wilson H.B. & Scharstein R.W.]');
                    disp(ka)

                    // for comparison with [2]
                    disp('Characteristic frequencies as in [Gutiérrez-Vega, J., S. Chávez-Cerda, and Ramón Rodríguez-Dagnino]');
                    disp(ws)
                 end
             end
        end

endfunction
