// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function y = mathieu_rad_fey( m, q, z, fun_or_der, Arm )
//MATHIEU_RAD_FEY Even radial (modified) Mathieu function of the second kind or its first derivative.
//   Function for calculation of even radial (modified) Mathieu function of the second kind 'Fey' or its
//   first derivative
//
//   Arguments:
//       m - function order
//       q - value of parameter q
//       z - argument
//    fun_or_der - calculate function (1) or derivative (0)
//      Arm - expansion coefficients for the same order m and value of q.
   
    // calculate Arm expansion coefficients inside or outside of this function
    if argn(2)<5
        // calculate expansion coefficients here for given m and q
        Arm = mathieu_Arm(m,q);
    end

    // calculation of Mathieu function 'Mc1' or its first derivative (AS 20.6.7-20.6.8 with 20.4.13)
            tMc = mathieu_rad_mc( m, q, z, fun_or_der, 2, Arm );

    // calculation of Mathieu function 'Fey' or its first derivative (like AS 20.6.15)
    if modulo(m,2) == 0 // 2r
            // multiplier p2n (ML p. 368 (3)): 
            // p2n = ce2n(0,q) * ce2n(%pi/2,q) / A0_2n
            fun = 1;
            p2n = mathieu_ang_ce(m, q, 0, fun, Arm) * mathieu_ang_ce(m, q, %pi/2, fun, Arm) / Arm(1);            
            mult = p2n / (-1) ^ (m/2);

    else // 2r+1
            // multiplier p2n1 (ML p. 439 (4)): 
            // p2n1 = -ce2n+1(0,q) * ce2n+1'(%pi/2,q) / (sqrt(q)*A1_2n)
            fun = 1; der = 0;
            p2n1 = -mathieu_ang_ce(m, q, 0, fun, Arm) * mathieu_ang_ce(m, q, %pi/2, der, Arm) / (sqrt(q) * Arm(1));
            mult = p2n1 / (-1) ^ ((m-1)/2);
    end
    
    // returning function of its derivative
    y = real(mult * tMc);    
endfunction
