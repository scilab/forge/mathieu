// Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
// Copyright (C) X. K. Yang
// Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GPL Version 2
// http://www.gnu.org/licenses/gpl-2.0.html

function [x, y, z] = mathieu_pol2cart (theta, rho, z)
    // mathieu_pol2cart (theta, rho, [z])
    // transform from polar (cylindrical) to Cartesian coordinates
    // 
    // output: Cartesian coordinates x,y,[z]
    // input: polar coordinates
    //        theta - angle
    //        rho   - radius
    //        z     - z height (cylindrical case)
    x = rho.*cos(theta);
    y = rho.*sin(theta);
endfunction
