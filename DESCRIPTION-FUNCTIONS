Coordinate transformation:
  mathieu_cart2ell    -   convert coordinates from Cartesian to elliptical
  mathieu_ell2cart    -   convert coordinates from elliptical to Cartesian
  mathieu_cart2pol    -   convert coordinates from Cartesian to polar
  mathieu_pol2cart    -   convert coordinates from polar to Cartesian
  mathieu_ell_in_pol  -   calculate polar coordinates of a point at known angle on ellipse

Floquet solution:
  mathieu_mathieuf    -   evaluate characteristic values and expansion coefficients
  mathieu_mathieu     -   evaluate periodic Mathieu functions by calling mathieu_mathieuf
  mathieu_mathieuexp  -   calculate the characteristic exponent of non-periodic solutions and the coefficients of the expansion of the periodic factor
  mathieu_mathieuS    -   calculate solutions of ME with arbitrary a and q parameters (uses mathieu_mathieuexp)

Solution in angular and radial (modified) functions:
  mathieu_Arm         -   compute expansion coefficients 'Arm' and eigenvalue 'am' for even angular and radial Mathieu functions
  mathieu_Brm         -   compute expansion coefficients 'Brm' and eigenvalue 'bm' for odd angular and radial Mathieu functions
  mathieu_ang_ce      -   compute even angular Mathieu function 'ce' or its first derivative
  mathieu_ang_se      -   compute odd angular Mathieu function 'se' or its first derivative
  mathieu_rad_mc      -   compute even radial (modified) Mathieu function 'Mc' or its first derivative (kinds 1 and 2)
  mathieu_rad_ms      -   compute odd radial (modified) Mathieu function 'Ms' or its first derivative (kinds 1 and 2)
  mathieu_rad_ce      -   compute even radial (modified) Mathieu function of the first kind 'Ce' or its first derivative
  mathieu_rad_se      -   compute odd radial (modified) Mathieu function of the first kind 'Se' or its first derivative
  mathieu_rad_fey     -   compute even radial (modified) Mathieu function of the second kind 'Fey' or its first derivative
  mathieu_rad_gey     -   compute odd radial (modified) Mathieu function of the second kind 'Gey' or its first derivative

Calculation modes of elliptical membrane:
  mathieu_rootfinder    - rootfinder for radial Mathieu function or its first derivative (finds q values of given radial function type with known order m and radial argument ξ0, which satisfies the equation RMF_m(q,xi0) = 0 (Dirichlet boundary condition) or RMF_m'(q,xi0) = 0 (Neumann boundary condition)
  mathieu_membrane_mode - calculate elliptical membrane mode for known semi-axes, mode numbers, mode type and boundary condition (Soft/Dirichlet or Hard/Neumann)

