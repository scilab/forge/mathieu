<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Copyright (C) 2001 - 2002 - R.Coisson & G. Vernizzi, Parma University
 * Copyright (C) X. K. Yang
 * Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 *
 * This file must be used under the terms of the GPL Version 2
 * http://www.gnu.org/licenses/gpl-2.0.html
-->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en_US" xml:id="mathieu_mathieu">
  <info>
    <pubdate>$LastChangedDate: 02-02-2011 $</pubdate>
  </info>
  <refnamediv>
    <refname>mathieu_mathieu</refname>
    <refpurpose>Evaluates periodic Mathieu functions.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[y, ab] = mathieu_mathieu(kind, n, z, q [, prec])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>kind</term>
        <listitem>
          <para>
            'ce', 'se', 'ce_' (ce first derivative), 'se_' (se first derivative), 'Ce', 'Se', ('Fy', 'Gy', 'Fk', 'Gk' not yet)
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>n</term>
        <listitem>
          <para>
            the order, an array of positive integers
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>z</term>
        <listitem>
          <para>
            the argument, a real row vector
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>q</term>
        <listitem>
          <para>
            parameter of the confocal ellipse family (q=0 : circle)
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>prec</term>
        <listitem>
          <para>
            (optional) the precision (default 1e-3)
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>y</term>
        <listitem>
          <para>
            value of corresponding Mathieu function
          </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>ab</term>
        <listitem>
          <para>
            the characteristic values with ordering
<math display="block" xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt">
<mrow>
  <msub>
          <mi>a</mi>
        <mn>0</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>a</mi>
        <mn>1</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>b</mi>
        <mn>1</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>b</mi>
        <mn>2</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>a</mi>
        <mn>2</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>a</mi>
        <mn>3</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>b</mi>
        <mn>3</mn>
  </msub>
  <mo>&lt;</mo>
  <mo>...</mo>
  <mspace width="0.5em" />
  <mi>for</mi>
  <mspace width="0.5em" />
  <mi>q</mi>
  <mo>&lt;</mo>
  <mn>0</mn>
  <mspace width="0.5em" />  
</mrow>
</math>
 and 
<math display="block" xmlns="http://www.w3.org/1998/Math/MathML" fontsize="20pt">
<mrow>
  <msub>
          <mi>a</mi>
        <mn>0</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>b</mi>
        <mn>1</mn>
  </msub>
  <mo>&#8804;</mo>
  <msub>
          <mi>a</mi>
        <mn>1</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>b</mi>
        <mn>2</mn>
  </msub>
  <mo>&#8804;</mo>
  <msub>
          <mi>a</mi>
        <mn>2</mn>
  </msub>
  <mo>&lt;</mo>
  <msub>
          <mi>b</mi>
        <mn>3</mn>
  </msub>
  <mo>&#8804;</mo>
  <msub>
          <mi>a</mi>
        <mn>3</mn>
  </msub>
  <mo>&lt;</mo>
  <mo>...</mo>
  <mspace width="0.5em" />
  <mi>for</mi>
  <mspace width="0.5em" />
  <mi>q</mi>
  <mo>&#8805;</mo>
  <mn>0</mn>
  <mspace width="0.5em" />  
</mrow>
</math>
 , 
for <emphasis>q</emphasis> complex, the ordering is not well-defined.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
      <link linkend="mathieu_mathieu">Mathieu_mathieu</link> evaluates periodic Mathieu functions by calling <link linkend="mathieu_mathieuf">mathieu_mathieuf</link>
    </para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example">
      <![CDATA[
// se1 functions with different values of q
// (see fig. 4 of [1])
z = linspace(0, 2*%pi, 100);
plot(z, mathieu_mathieu('se', 1, z, 0.1)', 'b', z, mathieu_mathieu('se', 1, z, 1)', 'g', ...
 z, mathieu_mathieu('se', 1, z, 5)', 'r', z, mathieu_mathieu('se', 1, z, 20)', 'c'); 
legend('$q = 0.1$','$q = 1$','$q = 5$','$q = 20$');
xgrid;
xtitle('$se_1\ \text{functions\ with\ different}\ q$','$z$','$se_1$');
h = gca(); font_sz = 4;
h.font_size = font_sz; h.title.font_size=font_sz; 
h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
h.children(1).font_size = font_sz;
]]>
    </programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="mathieu_mathieuf">mathieu_mathieuf</link>
      </member>
      <member>
        <link linkend="mathieu_mathieuexp">mathieu_mathieuexp</link>
      </member>
    </simplelist>
  </refsection>
  <refsection>
    <title>Used Functions</title>
    <para>besselj</para>
  </refsection>
  <refsection>
    <title>Authors</title>
    <para>
      R.Coisson and G. Vernizzi, <emphasis>Parma University</emphasis>
    </para>
    <para>
      X. K. Yang
    </para>
    <para>
      N. O. Strelkov, <emphasis>NRU MPEI</emphasis>
    </para>
    <para>
      2011 - DIGITEO - Michael Baudin
    </para>
  </refsection>
  <refsection>
     <title>Bibliography</title>
       <para>
		1. R. Coïsson, G. Vernizzi and X.K. Yang, "Mathieu functions and numerical solutions of the Mathieu equation", IEEE Proceedings of OSSC2009 (online at <ulink url="http://www.fis.unipr.it/~coisson/Mathieu.pdf">http://www.fis.unipr.it/~coisson/Mathieu.pdf</ulink>).
       </para>
   </refsection>
</refentry>
