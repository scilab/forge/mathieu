changelog of the Mathieu Functions Toolbox

Mathieu Functions Toolbox (v.4.0.61)
* NS: adapted toolbox for scilab-6.1 (used newest toolbox_skeleton, replaced 'xset', 'eval' -> 'evstr', added some more LaTeX to plots)

Mathieu Functions Toolbox (v.4.0)
* NS: updated overview help page
* NS: added GUI for Mathieu Functions Toolbox with four tabs - "Elliptic and Cartesian coordinates", "Angular Mathieu Functions", "Radial Mathieu Functions", "Elliptic membrane"
* NS: fixed typo in demos/ellissi.sce

Mathieu Functions Toolbox (v.3.5)
* NS: increased axis margins in demos (for low resolutions)
* NS: renamed unit-test and dia.ref files to have mathieu_ prefix.
* NS: update Overview help-page
* NS: small edits in help-pages (Copyrights, links, etc.)
* NS: rewrote demos/confroq.sce, demos/confroq4.sce, demos/ellissi.sce using new mathieu_membrane_mode function
* NS: removed mathieu_elliptical function as physically incorrect
* NS: created unit_test and help-page for mathieu_membrane_mode
* NS: created function mathieu_membrane_mode - calculate elliptical membrane mode for known semi-axes, mode numbers, mode type and boundary condition (Soft/Dirichlet or Hard/Neumann)
* NS: removed funcprot(0) calls
* NS: created unit_test and help-page for mathieu_rootfinder
* NS: created mathieu_rootfinder function - rootfinder for radial Mathieu function or its first derivative (finds q values of given radial function type with known order m and radial argument ξ0, which satisfies the equation RMF_m(q,xi0) = 0 (Dirichlet boundary condition) or RMF_m'(q,xi0) = 0 (Neumann boundary condition)
* NS: fixed error in demos/ell_cart.sce - wrong markings of 0 and 180 degrees
* NS: updated builder.sce to support Scilab 6.
* NS: added semicolons to radial unit-tests

Mathieu Functions Toolbox (v.3.1)
* NS: fixed LaTeX markup that was rendered wrong in Scilab >= 5.4.0
* NS: added LaTeX legend and removed italic from title in mathieu_mathieu test and help
* NS: fixed warning about transposing in mathieu_Brm test and help
* NS: fixed warning about transposing in mathieu_Arm test and help
* NS: fixed warning about transposing in mathieu_mathieu test and help
* NS: fixed error 999 - 'figure: No more memory' in demos (ce_se_plots_2d, ce_se_plots_3d, mc1_ms1_plots_3d, mc2_ms2_plots_3d, rad_ce_se_plots_3d, rad_fey_gey_plots_3d) - Scilab 5.5.0 specific
* NS: fixed check of number of input arguments in mathieu_rad_ce, mathieu_rad_fey, mathieu_rad_gey, mathieu_rad_se
* NS: adjusted tolerances in unit-tests of mathieu_Arm, mathieu_rad_ce, mathieu_rad_se for Scilab >=5.4.0
* NS: fixed typo in help (mathieu_ang_ce)
* NS: fixed typo in help (mathieu_cart2ell)

Mathieu Functions Toolbox (v.3.0.2)
* NS: ATOMS compatibility fixes, need MathML

Mathieu Functions Toolbox (v.3.0.1)
* NS: small fixes in doc and unit tests

Mathieu Functions Toolbox (v.3.0)
* NS: released v.3.0
* NS: small doc update
* NS: functional upgrade: added angular and radial Mathieu functions with demos, examples, tables, unit-tests, help
* NS: added example for mathieu_mathieuS
* NS: updated copyrights in tests
* NS: updated copyrights in macroses
* NS: updated docs
* NS: updated copyrights in demos
* NS: mathieu_cart2pol.xml: atan(y/x) -> atan(y,x) - 4-quadrant atan
* NS: more intuitive view of demos/ell_cart.sce plot
* NS: removed mathieu_ellipse as duplicate of mathieu_elliptical
* NS: removed mathieu_expqa as duplicate of mathieu_mathieuexp
* NS: mathieu_cart2ell function changed according to McLachlan; updated cart2ell help page; updated cart2ell unit-test
* NS: changed u->xi, v->eta, c->f in mathieu_ell2cart.sci; updated ell2cart unit-test; updated ell2cart help page
* NS: removed demos/provaell.sce
* NS: added xtitle and changed u->xi, v->eta to demos/ellissi.sce
* NS: added xtitles to demos/confroq4.sce
* NS: added xtitles to demos/confroq.sce
* NS: added mathieu_cart2pol function, its help and unit-test
* NS: added mathieu_pol2cart function, its help and unit-test
* NS: added mathieu_ell_in_pol  function, its help and unit-test
* NS: created demos/ell_cart.sce for demonstrating conversion between Cartesian and elliptical coordinates
* NS: updated style of mathieu_expqa help page
* NS: updated style of mathieu_mathieuS help page
* NS: changed u->xi, v->eta in mathieu_ellipse, mathieu_elliptical
* NS: updated help page and unit-test of mathieu_mathieu
* NS: updated help page and unit-test of mathieu_mathieuf
* NS: fixed the example in mathieu_mathieuexp, updated its unit-test

Mathieu Functions Toolbox (v2.0)
 * 23/05/2011: Source code uploaded to Scilab 
   Forge: http://forge.scilab.org/index.php/p/mathieu/
 * 16/05/2011: Changed directory structure as it required 
   by ATOMS packaging system. Removed temporary files.
 * 16/05/2011: Fixed 'error 15' in mathieuf example.
 * MB: Fixed the startup script name.
 * MB: Fixed the messages in the console.
 * MB: Fixed the encoding of the scripts, indented them.
 * MB: Updated the readme, the changelog.
 * MB: Simplified the descriptions of the functions.
 * MB: Added a description for the invellip function.
 * 03/06/2011:
 * NS: Renamed invellip to cart2ell
 * NS: Renamed all functions into mathieu_* to avoid name conflicts 
   (cart2ell->mathieu_cart2ell, 
   ell2cart->mathieu_ell2cart, 
   ellipse->mathieu_ellipse,
   elliptical->mathieu_elliptical,
   expqa->mathieu_expqa,
   mathieu->mathieu_mathieu, 
   mathieuexp->mathieu_mathieuexp, 
   mathieuf->mathieu_mathieuf, 
   mathieuS->mathieu_mathieuS), 
   updated help pages
 * MB: created unit tests.
 * MB: removed unnecessary calls to ieee and mode functions.
 * MB: Added licence headers to the macros.
 * NS: removed remained unnecessary calls to ieee and mode functions.
 * NS: changed TOOLBOX_TITLE
 * NS: fixed 'error 15' mathieuf.tst unit test.
 * MB: Fixed formatting of help pages.
 * MB: Added an example in mathieu_cart2ell.xml.
 * MB: Created an overview help page.
 * MB: Added licence header into mathieu_cart2ell.

Mathieu Functions Toolbox v0.02rc1_scilab5 02/02/2011
 * Toolbox file structure converted to ATOMS format. 
 * Help files converted from old Scilab 4.x to new Scilab 5.x format. 
 * Function 'sort' is removed from Scilab 5.3, changed to 'gsort' in file macros/mathieuf.sci:31.
 * Calculation of first derivatives of ce and se (ce_ and se_) were added to macros/mathieu.sci file. 
 * Characteristic values ar or br (calculated by mathieuf) added to output parameters of mathieu function.
 * Four demos where added. For more information read their source code - files demos/ce_se_plots_2d.sci, ce_se_plots_3d.sci, ce_tables.sci, se_tables.sci. 
 * All demos can be launched from "?->Scilab Demonstrations" menu.
 * Tested on Scilab 5.2 and 5.3 under Windows and GNU/Linux operation systems.
 * These changes were made by Nikolay Strelkov <n.strelkov@gmail.com>.

Mathieu Functions Toolbox v0.02rc1 (Thu Oct 4 9:14:50 CST 2007)
 * Welcome feedback on the bugs, usage, functions, etc on this toolbox.
 * yxkmlstar@gmail.com

Mathieu Functions Toolbox v0.02rc1 (10/03/2007)
 * This is a release candidate version. 
 * many more extra functions and demos.
 * added macros(i.e. the function can be called in scilab) are as follows:
   * elliptical: 3D plot of a mode with elliptical boundary conditions 
   * ellipse: 3D plot of a mode with elliptical boundary conditions 
   * invellip:
   * expqa:
 * the demo files added are as follows:
   * confroq.sce
   * confroq4.sce
   * provaell.sce
 * Known bugs
   * There is something wrong with expqa, which cannot be executed because singularities in the function.
   * The 3-D figures are drawn very slowly. I am not sure whether it is a bug of scilab or not.

Mathieu Functions Toolbox v0.01 (09/22/2007) : initial release
 * Welcome the initial release of Mathieu Equation Toolbox(metools)!
 * There are still some problems lies in this release, which will be fixed soon.
