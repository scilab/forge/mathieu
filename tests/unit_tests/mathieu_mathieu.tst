// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

[y ab]=mathieu_mathieu('ce',0,0,0);
assert_checkalmostequal(y,0.7071068,1.e-6);

[y ab]=mathieu_mathieu('ce_',1,%pi/2,0);
assert_checkalmostequal(y,-1,1.e-6);

[y ab]=mathieu_mathieu('se',1,%pi/2,0);
assert_checkalmostequal(y,1,1.e-6);

[y ab]=mathieu_mathieu('se_',2,%pi/2,0);
assert_checkalmostequal(y,-2,1.e-6);

// se1 functions with different values of q
// (see fig. 4 of [1])
z = linspace(0, 2*%pi, 100);
plot(z, mathieu_mathieu('se', 1, z, 0.1)', 'b', z, mathieu_mathieu('se', 1, z, 1)', 'g', ...
 z, mathieu_mathieu('se', 1, z, 5)', 'r', z, mathieu_mathieu('se', 1, z, 20)', 'c'); 
legend('$q = 0.1$','$q = 1$','$q = 5$','$q = 20$');
xgrid;
xtitle('$se_1\ \text{functions\ with\ different}\ q$','$z$','$se_1$');
h = gca(); font_sz = 4;
h.font_size = font_sz; h.title.font_size=font_sz; 
h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
h.children(1).font_size = font_sz;
