// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

y = mathieu_mathieuS(1,1,1,1);
assert_checkalmostequal(real(y), 0.8379111, 1.e-6);
assert_checkalmostequal(imag(y), - 0.1471302, 1.e-6);

// Example: test solution dependency on ndet
    z = %pi/2;
    q = 5;
    r = 1 ;
    
    [f,a]=mathieu_mathieu('ce', r, z, q);
    
    ndets_max = 20;
    S = zeros(1, ndets_max);
    ndets = 1:ndets_max;
    
    for ndet=ndets
        S(ndet) = mathieu_mathieuS(z,a,q,ndet);
    end
    
    plot(ndets, abs(S))
    xgrid
    xlabel('ndet')
    ylabel('y')
    xtitle('solution dependency on ndet')