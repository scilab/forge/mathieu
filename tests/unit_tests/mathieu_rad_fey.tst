// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//

// comparison with values from Tables 14.3, 14.5, 14.9 from Zhang & Jin
tol = 1e-5;

// Fey and Fey' ( calculating by formulas ~20.6.15 from Abramowitz-Stegun)
    m = 1; z = 0; q = 10; fun = 1; der = 0;
    ce2n1_p_90deg    = -4.85043830; // table 14.5(b), p. 527
    ce2n1_0deg       = 0.05359875;  // table 14.5(a), p. 527
    A1              = 5.6936745e-1;// table 14.3, p. 525
    Mc2             = -0.00090264; // table 14.9(a), p. 531
    Mc2_p           = 1.71501716;  // table 14.9(b), p. 531
    Fey_from_tables = - (ce2n1_p_90deg * ce2n1_0deg / A1) * Mc2 / sqrt(q);
    Fey_p_from_tables = - (ce2n1_p_90deg * ce2n1_0deg / A1) * Mc2_p / sqrt(q);

    assert_checkalmostequal(Fey_from_tables, mathieu_rad_fey(m, q, z, fun), tol);
    assert_checkalmostequal(Fey_p_from_tables, mathieu_rad_fey(m, q, z, der), tol);

    //m = 2, z = 0, q = 10
    m = 2; z = 0; q = 10; fun = 1; der = 0;
    ce2n_90deg      = -0.92675926; // table 14.5(a), p. 527
    ce2n_0deg       = 0.24588835;  // table 14.5(a), p. 527
    A0              = 4.0950957e-1;// table 14.3, p. 525
    Mc2             = -0.01903346; // table 14.9(a), p. 531
    Mc2_p           = 1.44073134;  // table 14.9(b), p. 531
    Fey_from_tables = - (ce2n_90deg * ce2n_0deg / A0) * Mc2;
    Fey_p_from_tables = - (ce2n_90deg * ce2n_0deg / A0) * Mc2_p;

    assert_checkalmostequal(Fey_from_tables, mathieu_rad_fey(m, q, z, fun), tol);
    assert_checkalmostequal(Fey_p_from_tables, mathieu_rad_fey(m, q, z, der), tol);

// Example: Fey0 and Fey1 (for comparison with Fig. 4c, p. 236 of the article J. C. Gutiérrez-Vega, et al. Mathieu functions, a visual approach [4]) with first derivative
    f=scf(); mrgn = 50; font_sz = 4;
    sz = get(0, 'screensize_px') + [-1 mrgn-1 0 -2*mrgn ] ;
    f.figure_name = 'Fey0 and Fey1 (for comparison with Fig. 4c, p. 236 of the article J. C. Gutiérrez-Vega, et al. Mathieu functions, a visual approach) with first derivative';

    z = linspace(0, 2, 100);
    fun = 1; der = 0;
    
    m = 0;
    subplot(2,2,1);
        plot(z, mathieu_rad_fey(m, 1, z, fun), 'r', z, mathieu_rad_fey(m, 2, z, fun), 'b', z, mathieu_rad_fey(m, 3, z, fun), 'g')
        xgrid
        ty1 = '$Fey_0(z,q)$'; xlabel('$z$'); ylabel(ty1);
        legend('$q = 1$', '$q = 2$', '$q = 3$');
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    subplot(2,2,3)
        plot(z, mathieu_rad_fey(m, 1, z, der), 'r-.', z, mathieu_rad_fey(m, 2, z, der), 'b-.', z, mathieu_rad_fey(m, 3, z, der), 'g-.')
        xgrid
        ty2 = '$Fey^{\ \prime}_0(z,q)$'; xlabel('$z$'); ylabel(ty2);
        legend('$q = 1$', '$q = 2$', '$q = 3$',pos=3);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    
    m = 1; 
    subplot(2,2,2);
        plot(z, mathieu_rad_fey(m, 1, z, fun), 'r', z, mathieu_rad_fey(m, 2, z, fun), 'b', z, mathieu_rad_fey(m, 3, z, fun), 'g')
        xgrid
        ty1 = '$Fey_1(z,q)$'; xlabel('$z$'); ylabel(ty1);
        legend('$q = 1$', '$q = 2$', '$q = 3$',pos=4);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    subplot(2,2,4)
        plot(z, mathieu_rad_fey(m, 1, z, der), 'r-.', z, mathieu_rad_fey(m, 2, z, der), 'b-.', z, mathieu_rad_fey(m, 3, z, der), 'g-.')
        xgrid
        ty2 = '$Fey^{\ \prime}_1(z,q)$'; xlabel('$z$'); ylabel(ty2);
        legend('$q = 1$', '$q = 2$', '$q = 3$',pos=3);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    f.figure_position=[sz(1) sz(2)]; f.figure_size=[sz(3) sz(4)]; 
