// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//

// comparison with Table 20.1 from A-S
tol = 5.e-8;

// functions
fun_or_der = 1;
    // m = 1
        // z = pi/2
        m = 1; q = 0; z = %pi/2;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.0, tol);
        
        m = 1; q = 25; z = %pi/2;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.6575104, tol);

    // m = 5
        // z = pi/2
        m = 5; q = 0; z = %pi/2;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.0, tol);
        
        m = 5; q = 25; z = %pi/2;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 8.9926833e-1, tol);

    // m = 15
        // z = pi/2
        m = 15; q = 0; z = %pi/2;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -1.0, tol);
        
        m = 15; q = 25; z = %pi/2;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -9.4670870e-1, tol);

// derivatives
fun_or_der = 0;
    // m = 2
        // z = 0
        m = 2; q = 0; z = 0;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 2.0, tol);
        
        m = 2; q = 25; z = 0;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.6056217e-2, tol);
        
        // z = pi/2
        m = 2; q = 0; z = %pi/2;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -2.0, tol);
        
        m = 2; q = 25; z = %pi/2;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -7.1067719, tol);
        
    // m = 10
        // z = 0
        m = 10; q = 0; z = 0;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.0e1, tol);
        
        m = 10; q = 25; z = 0;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 8.3526784, tol);
        
        // z = pi/2
        m = 10; q = 0; z = %pi/2;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -1.0e1, tol);
        
        m = 10; q = 25; z = %pi/2;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, -1.0941354e1, tol);
        
    // m = 1
        // z = 0
        m = 1; q = 0; z = 0;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.0, tol);
        
        m = 1; q = 25; z = 0;
        y = mathieu_ang_se( m, q, z, fun_or_der)
        assert_checkalmostequal(y, 2.0443594e-3, tol);
        
    // m = 5
        // z = 0
        m = 5; q = 0; z = 0;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 5.0, tol);
        
        m = 5; q = 25; z = 0;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 9.6407162e-1, tol);
        
    // m = 15
        // z = 0
        m = 15; q = 0; z = 0;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.5e1, tol);
        
        m = 15; q = 25; z = 0;
        y = mathieu_ang_se( m, q, z, fun_or_der);
        assert_checkalmostequal(y, 1.4064373e1, tol);
        
// Brm pass test
    m = 12;
    q = 34;
    z = %pi/4;
    tol = 1e-16;
    // function
    fun_or_der = 1; 
        Brm_ext = mathieu_Brm(m, q);
        y_int = mathieu_ang_se(m, q, z, fun_or_der);
        y_ext = mathieu_ang_se(m, q, z, fun_or_der, Brm_ext);
        assert_checkalmostequal(y_int, y_ext, tol);
    // derivative
    fun_or_der = 0; 
        Brm_ext = mathieu_Brm(m, q);
        y_int = mathieu_ang_se(m, q, z, fun_or_der);
        y_ext = mathieu_ang_se(m, q, z, fun_or_der, Brm_ext);
        assert_checkalmostequal(y_int, y_ext, tol);
        
// comparison with values from Tables 14.6(a) from Zhang & Jin
// p. 528
    tol = 1e-6;
    q = 10;
  // ce
    fun_or_der  = 1;
    // z = 0 deg
    z = 0;
        assert_checkalmostequal(mathieu_ang_se(1, q, z, fun_or_der), 0, tol);
        assert_checkalmostequal(mathieu_ang_se(2, q, z, fun_or_der), 0, tol);
        assert_checkalmostequal(mathieu_ang_se(3, q, z, fun_or_der), 0, tol);
        assert_checkalmostequal(mathieu_ang_se(4, q, z, fun_or_der), 0, tol);
        assert_checkalmostequal(mathieu_ang_se(5, q, z, fun_or_der), 0, tol);
   // z = 45 deg
   z = %pi / 4;
        assert_checkalmostequal(mathieu_ang_se(1, q, z, fun_or_der), 0.25026208, tol);
        assert_checkalmostequal(mathieu_ang_se(2, q, z, fun_or_der), 0.69165646, tol);
        assert_checkalmostequal(mathieu_ang_se(3, q, z, fun_or_der), 1.08170282, tol);
        assert_checkalmostequal(mathieu_ang_se(4, q, z, fun_or_der), 0.96023499, tol);
        assert_checkalmostequal(mathieu_ang_se(5, q, z, fun_or_der), 0.22999532, tol);
   // z = 90 deg
   z = %pi / 2;
        assert_checkalmostequal(mathieu_ang_se(1, q, z, fun_or_der), 1.46875566, tol);
        assert_checkalmostequal(mathieu_ang_se(3, q, z, fun_or_der), -0.95626214, tol);
        assert_checkalmostequal(mathieu_ang_se(5, q, z, fun_or_der), 0.84603843, tol);

// comparison with values from Tables 14.6(b) from Zhang & Jin
// p. 528
  // ce'
    fun_or_der = 0;
    // z = 0 deg
    z = 0;
        assert_checkalmostequal(mathieu_ang_se(1, q, z, fun_or_der), 0.04402257, tol);
        assert_checkalmostequal(mathieu_ang_se(2, q, z, fun_or_der), 0.24882284, tol);
        assert_checkalmostequal(mathieu_ang_se(3, q, z, fun_or_der), 0.85336591, tol);
        assert_checkalmostequal(mathieu_ang_se(4, q, z, fun_or_der), 1.98026447, tol);
        assert_checkalmostequal(mathieu_ang_se(5, q, z, fun_or_der), 3.40722676, tol);
        
   // z = 45 deg
   z = %pi / 4;
        assert_checkalmostequal(mathieu_ang_se(1, q, z, fun_or_der), 1.06522654, tol);
        assert_checkalmostequal(mathieu_ang_se(2, q, z, fun_or_der), 1.95397575, tol);
        assert_checkalmostequal(mathieu_ang_se(3, q, z, fun_or_der), 1.04866977, tol);
        assert_checkalmostequal(mathieu_ang_se(4, q, z, fun_or_der), -2.10802103, tol);
        assert_checkalmostequal(mathieu_ang_se(5, q, z, fun_or_der), -5.01186359, tol);
        
   // z = 90 deg
   z = %pi / 2;
        assert_checkalmostequal(mathieu_ang_se(2, q, z, fun_or_der), -4.86342207, tol);
        assert_checkalmostequal(mathieu_ang_se(4, q, z, fun_or_der), 5.31264970, tol);

// Example: Odd periodic Mathieu functions and their first derivatives
    f=scf(); mrgn = 50; font_sz = 4;
    sz = get(0, 'screensize_px') + [-1 mrgn-1 0 -2*mrgn ] ;
    f.figure_name = 'Odd periodic Mathieu functions and their first derivatives';

    z = linspace(0, 2*%pi, 100);
    fun = 1; der = 0;

    m = 1;
    subplot(2,2,1)
        plot(z, mathieu_ang_se(m,-3,z,fun), z, mathieu_ang_se(m,-1,z,fun), z, mathieu_ang_se(m,0,z,fun), z, mathieu_ang_se(m,1,z,fun), z, mathieu_ang_se(m,3,z,fun))
        xgrid
        legend('$q = -3$', '$q = -1$', '$q = 0$', '$q = 1$', '$q = 3$');
        ty = '$se_1(z,q)$'; xlabel('$z$'); ylabel(ty);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;

    subplot(2,2,3)
        plot(z, mathieu_ang_se(m,-3,z,der), z, mathieu_ang_se(m,-1,z,der), z, mathieu_ang_se(m,0,z,der), z, mathieu_ang_se(m,1,z,fun), z, mathieu_ang_se(m,3,z,der))
        xgrid
        legend('$q = -3$', '$q = -1$', '$q = 0$', '$q = 1$', '$q = 3$');
        ty = '$se^{\ \prime}_1(z,q)$'; xlabel('$z$'); ylabel(ty);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
        
    m = 2;
    subplot(2,2,2)        
        plot(z, mathieu_ang_se(m,-3,z,fun), z, mathieu_ang_se(m,-1,z,fun), z, mathieu_ang_se(m,0,z,fun), z, mathieu_ang_se(m,1,z,fun), z, mathieu_ang_se(m,3,z,fun))
        xgrid
        legend('$q = -3$', '$q = -1$', '$q = 0$', '$q = 1$', '$q = 3$');
        ty = '$se_2(z,q)$'; xlabel('$z$'); ylabel(ty);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;

    subplot(2,2,4)
        plot(z, mathieu_ang_se(m,-3,z,der), z, mathieu_ang_se(m,-1,z,der), z, mathieu_ang_se(m,0,z,der), z, mathieu_ang_se(m,1,z,fun), z, mathieu_ang_se(m,3,z,der))
        xgrid
        legend('$q = -3$', '$q = -1$', '$q = 0$', '$q = 1$', '$q = 3$');
        ty = '$se^{\ \prime}_2(z,q)$'; xlabel('$z$'); ylabel(ty);
        
        h = gca(); h.margins=[0.15 0.05 0.05 0.2]; h.font_size = font_sz - 1;  
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
	f.figure_position=[sz(1) sz(2)]; f.figure_size=[sz(3) sz(4)];
