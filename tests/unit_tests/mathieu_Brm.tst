// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//

// comparison with values from Tables 20.1 and 20.2 from M. Abramowitz and I. A. Stegun

[Brm, br] = mathieu_Brm(2,25);
assert_checkalmostequal(Brm(1), 0.657439912, 1.e-6);
assert_checkalmostequal(br, -21.31486062, 1.e-6);

[Brm, br] = mathieu_Brm(10,25);
assert_checkalmostequal(Brm(1), 0.018003596, 1.e-6);
assert_checkalmostequal(br, 103.22568004, 1.e-6);

[Brm, br] = mathieu_Brm(1,25);
assert_checkalmostequal(Brm(1), 0.813983846, 1.e-6);
assert_checkalmostequal(br, -40.25677898, 1.e-6);

[Brm, br] = mathieu_Brm(15,25);
assert_checkalmostequal(Brm(1), 0.000003717, 1.e-4);
assert_checkalmostequal(br, 226.40072004, 1.e-6);



// Table 20.1 3/4
    abs_tol = 1e-8;
    bs_r2 = [4 2.09946045 -2.38215824 -8.09934680 -14.49106325 -21.31486062];
    bs_r10 = [100 100.12636922 100.50676946 101.14517229 102.04839286 103.22568004];
    
    for r=[2, 10]
        i = 1;
        for q=0:5:25
            [Brm,b]=mathieu_Brm(r, q);
            bs_table = evstr(sprintf('%s%1d', 'bs_r', r));
            b_table = bs_table(i);
            assert_checkalmostequal(b_table, b, abs_tol);
            i = i + 1;
        end
    end
    
// Table 20.1 4/4
    bs_r1 = [1 -5.79008060 -13.93655248 -22.51300350 -31.31338617 -40.25677898];
    bs_r5 = [25 25.51081605 26.76642636 27.96788060 28.46822133 28.06276590];
    bs_r15 = [225 225.05581248 225.22335698 225.50295624 225.89515341 226.40072004];
    
    for r=[1, 5, 15]
        i = 1;
        for q=0:5:25
            [Brm,b]=mathieu_Brm(r, q);
            bs_table = evstr(sprintf('%s%1d', 'bs_r', r));
            b_table = bs_table(i);
            assert_checkalmostequal(b_table, b, abs_tol);
            i = i + 1;
        end
    end

// comparison with values from Tables 14.2 from Zhang & Jin
// p. 523
    q = 50;
    tol = 1e-7;    
    [Brm, br] = mathieu_Brm(1,q);
    assert_checkalmostequal(br, -86.1125385, tol);
    
    [Brm, br] = mathieu_Brm(2,q);
    assert_checkalmostequal(br, -58.8674030, tol);
    
    [Brm, br] = mathieu_Brm(3,q);
    assert_checkalmostequal(br, -32.7177605, tol);
    
    [Brm, br] = mathieu_Brm(4,q);
    assert_checkalmostequal(br, -7.7449364, tol);
    
    [Brm, br] = mathieu_Brm(5,q);
    assert_checkalmostequal(br, 15.9459632, tol);
    
// p. 522
  // q = 1
  q = 1;
  tol = 1e-6;
    [Brm, br] = mathieu_Brm(1,q);
    assert_checkalmostequal(br, -0.110249, 10*tol);
    
    [Brm, br] = mathieu_Brm(20,q);
    assert_checkalmostequal(br, 400.001253, tol);

  // q = 10
  q = 10;
  tol = 1e-6;  
    [Brm, br] = mathieu_Brm(1,q);
    assert_checkalmostequal(br, -13.936552, tol);
    
    [Brm, br] = mathieu_Brm(20,q);
    assert_checkalmostequal(br, 400.125338, tol);
    
  // q = 100
  q = 100;
  tol = 1e-6;  
    [Brm, br] = mathieu_Brm(1,q);
    assert_checkalmostequal(br, -180.253249, tol);
    
    [Brm, br] = mathieu_Brm(20,q);
    assert_checkalmostequal(br, 412.796652, tol);
    
  // q = 200
  q = 200;
  tol = 1e-6;  
    [Brm, br] = mathieu_Brm(1,q);
    assert_checkalmostequal(br, -371.967999, tol);
    
    [Brm, br] = mathieu_Brm(20,q);
    assert_checkalmostequal(br, 455.159230, tol);
    
// comparison with values from Tables 14.4 from Zhang & Jin
// p. 526

q = 10;
tol = 1e-6;
    // m = 1
    [Brm, br] = mathieu_Brm(1,q);
    assert_checkalmostequal(Brm(1:10), [8.9028653e-1, -4.3949462e-1, 1.1776261e-1, -1.9032372e-2,  2.0205838e-3, ...
                                       -1.5035374e-4,  8.2377736e-6, -3.4524564e-7, 1.1406681e-8, -3.0440816e-10]', tol);
    // m = 8
    [Brm, br] = mathieu_Brm(8,q);
    assert_checkalmostequal(Brm(1:10), [1.1762307e-2,  7.1515342e-2, 3.3723571e-1,  8.9973836e-1, -2.6521703e-1, ...
                                       3.3813907e-2, -2.5876243e-3, 1.3561026e-4, -5.2379192e-6,  1.5637405e-7]', tol);

// Example: Expansion coefficients Arm as continous functions of q
    f=scf(); mrgn = 50; font_sz = 4; f.figure_name='Expansion coefficients Brm as continous functions of q';
    sz = get(0, 'screensize_px') + [-1 mrgn-1 0 -2*mrgn ] ;

    q = linspace(0, 40, 40);
    Brm2n = zeros(6, length(q));
    Brm2n1 = zeros(6, length(q));
    for i=1:length(q)
        B2ns = mathieu_Brm(1, q(i));
        B2n1s = mathieu_Brm(2, q(i));
        Brm2n(1:6, i) = B2ns(1:6);
        Brm2n1(1:6, i) = B2n1s(1:6);
    end
    subplot(1,2,1)
        plot(q', Brm2n1');
        legend('$B_1^{(1)}$', '$B_3^{(1)}$', '$B_5^{(1)}$', '$B_7^{(1)}$', '$B_9^{(1)}$', '$B_{11}^{(1)}$');
        xlabel('$q$');
        ylabel('$B_m^{(1)}$');
        xgrid
        h = gca(); h.margins=[0.15 0.05 0.05 0.12]; h.font_size = font_sz - 1;
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    subplot(1,2,2);
        plot(q', Brm2n');
        legend('$B_0^{(2)}$', '$B_2^{(2)}$', '$B_4^{(2)}$', '$B_6^{(2)}$', '$B_8^{(2)}$', '$B_{10}^{(2)}$');
        xlabel('$q$');
        ylabel('$B_m^{(2)}$');
        xgrid
        h = gca(); h.margins=[0.15 0.05 0.05 0.12]; h.font_size = font_sz - 1;
        h.x_label.font_size=font_sz; h.y_label.font_size=font_sz;
        h.children(1).font_size = font_sz;
    f.figure_position=[sz(1) sz(2)];
    f.figure_size=[sz(3) sz(4)]; 
