// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2010 - 2012 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

[x, y]=mathieu_pol2cart(%pi/4, 1);
assert_checkalmostequal(x, 0.7071068, 1.e-6);
assert_checkalmostequal(y, 0.7071068, 1.e-6);

// draw a unit circle
theta = linspace(0, 2*%pi, 100);
[x, y] = mathieu_pol2cart(theta, 1);
polarplot(theta, sqrt(x.^2+y.^2));
plot(x, y, 'c:'); xgrid; h=gca(); h.isoview='on';
xtitle('From polar to Cartesian coordinates (unit circle)', 'x', 'y');
legend('polar','Cartesian');
