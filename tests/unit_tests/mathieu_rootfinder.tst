// Copyright (C) 2013-2016 - N. O. Strelkov, NRU MPEI
//
// This unit test compares mathieu_rootfinder results with known publications:
//
// 1. Wilson, Howard B., and Robert W. Scharstein. "Computing elliptic membrane high frequencies by Mathieu and Galerkin methods." Journal of Engineering Mathematics 57.1 (2007): 41-55. (online at http://scharstein.eng.ua.edu/ENGI1589.pdf or http://dx.doi.org/10.1007/s10665-006-9070-1 )
// 2. Neves, Armando GM. "Eigenmodes and eigenfrequencies of vibrating elliptic membranes: a Klein oscillation theorem and numerical calculations." Comm. Pure Appl. Anal. 2009. (online at http://www.ma.utexas.edu/mp_arc/c/09/09-174.pdf )
// 3. N.W. McLachlan, Theory and Application of Mathieu Functions, Oxford Univ. Press, 1947.
// 4. Shibaoka, Yoshio, and Fusako Iida. "On the free oscillation of water in a lake of elliptic boundary." The Journal of the Oceanographical Society of Japan. 21.3 (1965): 103-108. (online at http://www.terrapub.co.jp/journals/JO/JOSJ/pdf/2103/21030103.pdf )
// 5. Hamidzadeh, Hamid R., and L. Moxey. "Analytical modal analysis of thin-film flat lenses." Proceedings of the Institution of Mechanical Engineers, Part K: Journal of Multi-body Dynamics 219.1 (2005): 55-59.
// 6. Lee, W. M. "Natural mode analysis of an acoustic cavity with multiple elliptical boundaries by using the collocation multipole method." Journal of Sound and Vibration 330.20 (2011): 4915-4929.
// 7. Gutiérrez-Vega, J., S. Chávez-Cerda, and Ramón Rodríguez-Dagnino. "Free oscillations in an elliptic membrane." Revista Mexicana de Fisica 45.6 (1999): 613-622. (online at http://optica.mty.itesm.mx/pmog/Papers/P001.pdf )

ieee_mod_old = ieee();
ieee(2);
// local functions
    // from http://sourceforge.net/p/sivp/code/287/tree/trunk/sivp/macros/corr2.sci
    function [c] = corr2(imA, imB)
        if ( (size(size(imA), 2) >=3) | (size(size(imB), 2) >=3)) then
            error("The input must be 2D matrix.");
        end
        //check the image width and height
        if( or( size(imA)<>size(imB) )) then
            error("The two inputs do not have the same size.");
        end
    
        difA = double(imA) - mean2(imA);
        difB = double(imB) - mean2(imB);
        c = sum( difA.*difB ) / sqrt(sum(difA.^2) * sum(difB.^2));
    endfunction 
    
    // from http://sourceforge.net/p/sivp/code/287/tree/trunk/sivp/macros/mean2.sci
    function [m] = mean2(im) 
        if (size(size(im), 2) >=3) then
            error("The input must be 2D matrix.");
        end
        m = mean(double(im(:)));
    endfunction 
    
    // calculate 'ka' from 'a' and 'b'
    function ka_out = ka(a,b,qs)
        ka_out = ((2*sqrt(qs))/sqrt(1-(b/a)^2))/a;
    endfunction

    // calculate 'k' from 'q' and 'f'
    function k = k_from_q(f,q)
        k=2*sqrt(q)/f;
    endfunction

// assert tolerance
    tol = 1e-6;

// 1. // Comparison with [1, Table 5(a)] (and [2, Table 1])
  do_print = 0;
  do_plot = 0;

  // thresholds for accuracy lose
  nrn_thr = 0.00001;
  cc_thr = 1-0.00000001;

  // function tolerance
  q_fun_tol = 0.000000000001;

  xi0 = 2;
  a = cosh(xi0);
  b = sinh(xi0);

  // radial functions root values from [1, Table 5(a)]
  kaCe0 = [0.65123129,1.49784709,2.35503473,3.21749355,4.08203631,4.94732666,5.81293426];
  kaCe1 = [1.02808007,1.88387435,2.73593614,3.59120969,4.45108137,5.31394325,6.17818689];
  kaSe1 = [1.04707973,1.9183559,2.78411713,3.64924648,4.51454935,5.38008827,6.24580297];
  kaCe2 = [1.38747716,2.2688269,3.12671219,3.97901777,4.83233042,5.68947135,6.5501742];
  kaSe2 = [1.390691,2.28073785,3.15122897,4.01676291,4.88119341,5.74569041,6.61051077];
  kaCe3 = [1.72602682,2.63820326,3.51256008,4.37137203,5.22392323,6.07591028,6.93070787];
  kaSe3 = [1.72633659,2.64047266,3.52057045,4.38936664,5.25397302,6.11736398,6.98077514];
  kaCe4 = [2.0532671,2.99268176,3.88482152,4.75670224,5.61649374,6.46963032,7.32089967];
  kaSe4 = [2.05328995,2.99296217,3.8863912,4.76219234,5.62975373,6.49354625,7.35599793];
  kaCe5 = [2.37354289,3.33789876,4.24575752,5.13015765,6.00083943,6.86162875,7.71565472];
  kaSe5 = [2.37354435,3.33792596,4.24597533,5.13123875,6.00462084,6.8713373,7.73452314];
  kaCe6 = [2.68877579,3.67658787,4.59920934,5.49462815,6.37490193,7.24488262,8.10659696];
  kaSe6 = [2.68877588,3.67659013,4.59923384,5.49478863,6.37564532,7.24748867,8.11363424];


  funs = [cellstr('Ms1'),cellstr('Mc1'),cellstr('Se'),cellstr('Ce')];

  printf('Comparison with Wilson H. B., Scharstein R. W. [1, 2, Table 5(a)]:\n');

  figure('BackgroundColor',[1,1,1]);

  for f = 1:size(funs,"*")
    // function name
    func_name = char(funs(f));

    printf("%s\n",func_name);
  
    // row-variable with values from [1, Table 5(a)] for corresponding RMF
    func_name_var = func_name;
    if func_name=="Ms1" then
      func_name_var = "Se";
    elseif func_name=="Mc1" then
      func_name_var = "Ce";
    end
  
    // orders for Ms1 and Se starts from 1
    o_start = 0;
    if func_name_var=="Se" then
      o_start = 1;
    end
  
    subplot(fix(sqrt(max(size(funs)))), fix(sqrt(max(size(funs)))), f);
  
    // loop over orders
    for o = o_start:6
      m = o;

      printf("\t m = %d\t",m);
    
      // rootfinding for q-values
      qs = mathieu_rootfinder(m,xi0,0,0.1,7,q_fun_tol,1000,func_name,1,do_print,do_plot)';
      plot(qs,".-");
      set(gca(),"grid",[1,1]);
      set(gca(),"auto_clear","off");
      xlabel("root number");
      ylabel("q");
      title(func_name);
    
      // compare ka values from [1, Table 5(a)] and from mathieu_rootfinder
      wilson_ = evstr("ka"+func_name_var+string(m));
      scilab_ = ka(a, b, qs);
    
      // calculate correlation coefficient (CC) and normresnorm (NRN)
      cc = corr2(wilson_,scilab_);
      nrn = sqrt(sum((wilson_ - scilab_) .^2))/max(size(scilab_));
    
      // mark values with low CC ...
      cc_warn = " :)";

      cc_expr = (cc < cc_thr);
      assert_checkfalse(cc_expr);
      if cc_expr then
        cc_warn = "!!!";
      end
    
      // ... high NRN
      nrn_warn = " :)";
      
      nrn_expr = (nrn > nrn_thr);
      assert_checkfalse(nrn_expr);
      if nrn_expr then
        nrn_warn = "!!!";
      end
    
      // print CC and NRN
      printf("\t\t cc = %1.6e %s\t",cc,cc_warn);
      printf("\tnrn = %1.3e %s\n",nrn,nrn_warn);
      wm = [wilson_;scilab_];
    
    end

    printf("\n");
  end


// 2. // Comparison with [3]
  // function tolerance
  q_fun_tol = 1.000000000D-16;

  q_start = 1e-4;
  q_delta = 0.05;
  q_maxiter = 20;
  q_roots_tot = 1;

  // Comparison with [3, p. 299]
  //    Finding first root of Ce1(xi,q11)=0
  //    e = 0.8
  //    a = 5 cm, b = 3 cm, b/a = 3/5 = 0.6
  //    cosh(xi0) = 1/e = 1.25
  //    xi0 = arccosh(1/e) = arccosh(1.25) = 0.6931
  //
  //    Root:
  //         q11 = 3.144
  xi0 = acosh(1.25);
  
  printf("\nComparison with N.W. McLachlan [3, p. 299], Ce1(xi,q)=0:\n");
  func_name = 'Ce';
  fun_or_der = 1;
  
  q11 = 3.144;
  m = 1;

  qs = mathieu_rootfinder(m,xi0,q_start,q_delta,q_roots_tot,q_fun_tol,100,func_name,fun_or_der,do_print,do_plot);

  printf("\txi0 = %f \n",xi0);
  printf("\tq11   (book) = %f\n",q11);
  printf("\tq11 (Scilab) = %f\n",qs(1));
  delta = (100*(q11 - qs(1)))/q11;
  printf("\tdelta = %f %%\n", delta);
  assert_checkalmostequal(delta, -6.62356865049955, tol);

  // Comparison with [3, p. 302]
  //    Finding first root of derivative Ce1`(xi, q11)=0
  //    e = 0.8
  //    b/a = (1-e^2)^0.5 = 0.6
  //    cosh(xi0) = 1/e = 1.25
  //    xi0 = arccosh(1/e) = arccosh(1.25) = 0.6931
  //    
  //    Root:
  //         q11 ~= 0.56
  // 
  printf("\nComparison with N.W. McLachlan [3, p. 302], Ce1`(xi,q)=0:\n");
  func_name = 'Ce';
  fun_or_der = 0;


  q11 = 0.56;
  m = 1;

  qs = mathieu_rootfinder(m,xi0,q_start,q_delta,q_roots_tot,q_fun_tol,q_maxiter,func_name,fun_or_der,do_print,do_plot);

  printf("\txi0 = %f \n",xi0);
  printf("\tq11   (book) = %f\n",q11);
  printf("\tq11 (Scilab) = %f\n",qs(1));
  delta = (100*(q11 - qs(1)))/q11;
  printf("\tdelta = %f %%\n", delta);
  assert_checkalmostequal(delta, 0.2770571603166, tol);

  // Comparison with [3, p. 304]
  //    Finding first root of derivative Se1`(xi, q11)=0
  //    e = 0.8
  //    b/a = (1-e^2)^0.5 = 0.6
  //    cosh(xi0) = 1/e = 1.25
  //    xi0 = arccosh(1/e) = arccosh(1.25) = 0.6931
  //    
  //    Root:
  //         q11 ~= 1.4
  // 
  printf("\nComparison with N.W. McLachlan [3, p. 304], Se1`(xi,q)=0:\n");
  func_name = "Se";
  fun_or_der = 0;

  q11 = 1.4;
  q_maxiter = 100;

  qs = mathieu_rootfinder(m,xi0,q_start,q_delta,q_roots_tot,q_fun_tol,q_maxiter,func_name,fun_or_der,do_print,do_plot);

  printf("\txi0 = %f \n",xi0);
  printf("\tq11   (book) = %f\n",q11);
  printf("\tq11 (Scilab) = %f\n",qs(1));
  delta = (100*(q11 - qs(1)))/q11;
  printf("\tdelta = %f %%\n", delta);
  assert_checkalmostequal(delta, - 1.7667804609587, tol);

  // Comparison with [3, p. 308, Table 24]
  // Finding first roots of Ce1`(xi,q)=0 - Ce5`(xi0,q)=0
  //    e -> 1
  //    xi0 -> 0
  //    
  //    Roots:
  //          0.88982, 3.039074, 6.425862, 11.047993, 16.90474
  // 

  printf("\nComparison with McLachlan [3, p. 308, Table 24] with e -> 1, Ce1`(xi,q)=0 to Ce5`(xi,q)=0:\n");
  func_name = "Ce";
  fun_or_der = 0;

  e = 0.999999999;
  xi0 = acosh(1/e);
  q_ml = [0.88982,3.039074,6.425862,11.047993,16.90474];

  q_roots_tot = 1;
  q_maxiter = 1000;

  for m = 1:5
    printf("   m = %d, ",m);
    printf("xi0 = %f \n",xi0);

    qs = mathieu_rootfinder(m,xi0,q_start,q_delta,q_roots_tot,q_fun_tol,q_maxiter,func_name,fun_or_der,do_print,do_plot);
  
    printf("\tq%1d1   (book) = %f \n",m,q_ml(m));
    printf("\tq%1d1 (Scilab) = %f \n",m,qs(1));
    delta(m) = (100*(q_ml(m) - qs(1)))/q_ml(m);
    printf("\tdelta = %f %% \n", delta(m));
  end
  assert_checkalmostequal(delta, [0.0000009055970; 0.0000110365115; - 0.0000201002928; 0.0000008264007; - 0.0000089524668], tol*1000);

// 3. // Comparison with [4, p. 105, Table 1]
  // 
  // Finding roots of derivative Ce0`(xi,q) = 0
  // 
  // a/b = 5/4  e = 0.600  q = 1.785
  // a/b = 2    e = 0.866  q = 8.57
  // a/b = 3    e = 0.943  q = 21.43
  // 
  printf("\n\nComparison with Y. Shibaoka and F. Iida [4, p. 105, Table 1], Ce0`(xi,q)=0:\n");
  func_name = "Ce";
  fun_or_der = 0;


  rho_article = [5/4,2,3];
  q_article = [1.785,8.57,21.43];

  m = 0;
  q_start = 0.1;
  q_roots_tot = 1;
  q_maxiter = 1000;

  for i = 0:length(rho_article)-1
    rho = rho_article(i+1);
  
    e = sqrt(1-rho^(-2));
    xi0 = acosh(1/e);
  
    printf("\n   xi0 = %f \n",xi0);
  
    qs = mathieu_rootfinder(m,xi0,q_start,q_delta,q_roots_tot,q_fun_tol,q_maxiter,func_name,fun_or_der,do_print,do_plot);
  
    printf("\tq%1d1 (article) = %f \n",0,q_article(i+1));
    printf("\tq%1d1  (Scilab) = %f \n",0,qs(1));
    delta(i+1) = (100*(q_article(i+1) - qs(1)))/q_article(i+1);
    printf("\tdelta = %f %% \n", delta(i+1));
  end
  assert_checkalmostequal(delta, [- 0.0128789764225; 0.0125215087333; - 1.7259395188702; 0.0000008264007; - 0.0000089524668 ], tol*1000);

// 4. // Comparison with rough roots from [5, p. 58]
   printf('\n\nComparison with rough roots from H. R. Hamidzadeh and L. Moxey [5, p. 58]:\n');
    a = 5.74;
    b = 4.17;
    h = sqrt(a^2 - b^2);//3.945;
    e = h/a;//0.687;
    xi0 = 0.9203;acosh(1/e);//
    
    q_start = 0;
    q_delta = 0.05;
    q_roots_tot = 1;
    q_fun_tol = 1e-12;
    q_maxiter = 1000;

    fun_or_der = 1;
    do_print = 0;
    do_plot = 0;


    q_ce_article = [0.9967, 2.0506, 3.2729];
    q_se_article = [0, %nan, 3.9323];

    for m=0:2
        func_name = 'Ce';
        printf('\n  %s%d\n',func_name,m);
        q = mathieu_rootfinder(m,xi0,q_start,q_delta,q_roots_tot,q_fun_tol,q_maxiter,func_name,fun_or_der,do_print,do_plot);
        printf('\t  Expected (article): %f\n', q_ce_article(m+1));
        printf('\t Calculated (Scilab): %f\n', q);
        delta_ce(m+1) = 100*(q-q_ce_article(m+1))/q_ce_article(m+1);
        if ~isnan(delta_ce(m+1)) then
            printf('\t     Delta: %2.1f %%\n', delta_ce(m+1));
        end
        if m==2 then
            func_name = 'Se';
            printf('\n  %s%d\n',func_name,m);
            q = mathieu_rootfinder(m,xi0,q_start,q_delta,q_roots_tot,q_fun_tol,q_maxiter,func_name,fun_or_der,do_print,do_plot);
            printf('\t  Expected (article): %f\n', q_se_article(m+1));
            printf('\t Calculated (Scilab): %f\n', q);
            delta_se(m+1) = 100*(q-q_se_article(m+1))/q_se_article(m+1);
            printf('\t     Delta: %2.1f %%\n', delta_se(m+1));
        end
    end
    assert_checkalmostequal(delta_ce, [ - 0.9182398037513; 3.1523415551494; 14.553473935762], tol);
    assert_checkalmostequal(delta_se(3), 13.975686420138, tol);

// 5. // Comparison with [6, Fig. 9]
    printf('\n\nComparison with analytically found roots of Mc1` and Ms1` from W.M. Lee [6, Fig. 9]:\n');
    a = 1.0;
    b = 0.5;
    f=sqrt(a^2-b^2);
    xi0 = atanh(b/a);
    fun_or_der = 0;
    
        q_start = 0;
        q_delta = 0.05;
        q_roots_tot = 1;
        q_fun_tol = 1e-12;
        q_maxiter = 1000;

    k_Mc1 = [1.8736 3.4191 4.9292 6.4182];
    k_Ms1 = [3.5355 4.6414 5.8354 ];
    
    for im=1:4
        printf('  Mc(1)_%d\n', im)
        func_name = 'Mc1';
        q = mathieu_rootfinder(im,xi0,q_start,q_delta,q_roots_tot,q_fun_tol,q_maxiter,func_name,fun_or_der,do_print,do_plot);
        k = k_from_q(f,q);
        
        printf('\t  Expected (article): %f\n', k_Mc1(im));
        printf('\t Calculated (Scilab): %f\n', k);
        delta_mc(im) = 100*(k-k_Mc1(im))/k_Mc1(im);
        printf('\t     Delta: %2.2e %%\n', delta_mc(im));
        
        if im<4 then
            func_name = 'Ms1';
            printf('  Ms(1)_%d\n', im)
            
            q = mathieu_rootfinder(im,xi0,q_start,q_delta,q_roots_tot,q_fun_tol,q_maxiter,func_name,fun_or_der,do_print,do_plot);
            k = k_from_q(f,q);
            
            printf('\t  Expected (article): %f\n', k_Ms1(im));
            printf('\t Calculated (Scilab): %f\n', k);
            delta_ms(im) = 100*(k-k_Ms1(im))/k_Ms1(im);
            printf('\t     Delta: %2.2e %%\n\n', delta_ms(im));
        end
    end
    assert_checkalmostequal(delta_mc, [- 0.0013007464223; - 0.0020089601451; - 0.0027189889197; - 0.0028411705381], tol);
    assert_checkalmostequal(delta_ms, [- 0.0028307309861; - 0.0033281468596; - 0.0040297464402], tol);

// 6. // Comparison with [7, pp. 618-619]
    printf('\n\nComparison with [7, pp. 618-619] - roots of Mc1 and  Ms1:\n');
    //      Mc1 m=0: q01 = 1.735, q02 = 11.356 (p. 618, under Eq. 53)
    //      Mc1 m=1: q11 = 3.352, q12 = 14.627 (p. 619, in 5.3.1)
    //    
    //      Ms1 m=1: q11 = 5.430,  q12 = 19.478 (p. 619, in 5.3.2)
    a = 0.05;
    b = 0.03;
    
    f = sqrt(a^2 - b^2);
    e = f/a;
    
    [xi0, eta_tmp] = mathieu_cart2ell(a,0,f);    
    
    // settings
    do_print = 0;
    do_plot = 0;

    q_start = 0.001;
    q_delta = 0.05;
    q_roots_tot = 2;
    q_fun_tol = 1e-12;
    q_maxiter = 1e4;
    fun_or_der = 1;
    
    ms = [0 1 1];
    func_names = ['Mc1', 'Mc1', 'Ms1'];
    qs_expected = [1.735 11.356; 3.352 14.627; 5.430 19.478];
    
    for i=1:length(ms)
            qs_calculated = mathieu_rootfinder(ms(i), xi0, q_start, q_delta, q_roots_tot, q_fun_tol, q_maxiter, func_names(i), fun_or_der, do_print, do_plot)';
        for j=1:2
            printf('  %s(1)_%d, q_%d_%d\n', func_names(i), ms(i), ms(i), j);
            printf('\t  Expected (article): %f\n', qs_expected(i,j));
            printf('\t Calculated (Scilab): %f\n', qs_calculated(j));
            delta_q_error(i,j) = 100*(qs_calculated(j)-qs_expected(i,j))/qs_expected(i,j);
            printf('\t     Delta: %2.2e %%\n\n', delta_q_error(i,j));
        end
    end
    assert_checkalmostequal(delta_q_error, [0.0177502586813, 0.0033318311959; 0.0073090206360, 0.0028473312714; 0.0014020037460, 0.0024989572163], tol);

// Example: 
    // Comparison with [4, p. 105, Table 1]
      // 
      // Finding roots of derivative Ce0`(xi,q) = 0
      // 
      // a/b = 5/4  e = 0.600  q = 1.785
      // a/b = 2    e = 0.866  q = 8.57
      // a/b = 3    e = 0.943  q = 21.43
      //  
      printf("\nComparison with Y. Shibaoka and F. Iida [4, p. 105, Table 1], Ce0`(xi,q)=0:\n");
      func_name = "Ce";
      m = 0;
      fun_or_der = 0;

      rho_article = [5/4,2,3];
      q_article = [1.785,8.57,21.43];

      q_start = 0.1;
      q_delta = 0.05;
      q_roots_tot = 1;
      q_fun_tol = 1e-16;
      q_maxiter = 1000;
    
      do_print = 0; // set to 0 to pass unit_test
      do_plot = 1;

      for i = 0:length(rho_article)-1
        rho = rho_article(i+1);
      
        e = sqrt(1-rho^(-2));
        xi0 = acosh(1/e);
      
        printf("\n   xi0 = %f \n",xi0);
      
        qs = mathieu_rootfinder(m,xi0,q_start,q_delta,q_roots_tot,q_fun_tol,q_maxiter,func_name,fun_or_der,do_print,do_plot);
      
        printf("\tq%1d1 (article) = %f \n",0,q_article(i+1));
        printf("\tq%1d1  (Scilab) = %f \n",0,qs(1));
        delta(i+1) = (100*(q_article(i+1) - qs(1)))/q_article(i+1);
        printf("\tdelta = %f %% \n", delta(i+1));
      end

// set old value of ieee
ieee(ieee_mod_old)
