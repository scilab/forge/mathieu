// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

a = 5; b = 4; f = sqrt(a^2 - b^2);
xi = real(acosh(a/f));

[x,y] = mathieu_ell2cart(xi, 0, f);
assert_checkalmostequal(x, a, 1.e-6);
assert_checkalmostequal(y, 0, 1.e-6);

[x,y] = mathieu_ell2cart(xi, %pi/2, f);
assert_checkalmostequal(x, 0, 1.e-6, 1.e-2);
assert_checkalmostequal(y, b, 1.e-6);

[x,y] = mathieu_ell2cart(xi, %pi, f);
assert_checkalmostequal(x, -a, 1.e-6);
assert_checkalmostequal(y, 0, 1.e-6, 1.e-2);

[x,y] = mathieu_ell2cart(xi, 3*%pi/2, f);
assert_checkalmostequal(x, 0, 1.e-6, 1.e-2);
assert_checkalmostequal(y, -b, 1.e-6);

// parameters of ellipse
a = 5; b = 4; f = sqrt(a^2-b^2);
angle = 0:0.01*%pi:2*%pi;
// calculate elliptic coordinates of 4 points on ellipse
[xi, etaA] = mathieu_cart2ell(a, 0, f);
[xi, etaB] = mathieu_cart2ell(0, b, f);
[xi, etaC] = mathieu_cart2ell(-a, 0, f);
[xi, etaD] = mathieu_cart2ell(0, -b, f);

// convert elliptical coordinates to Cartesian
[x_el, y_el] = mathieu_ell2cart(xi, angle, f);
// calculate Cartesian coordinates of 4 points
[xA, yA] = mathieu_ell2cart(xi, etaA, f);
[xB, yB] = mathieu_ell2cart(xi, etaB, f);
[xC, yC] = mathieu_ell2cart(xi, etaC, f);
[xD, yD] = mathieu_ell2cart(xi, etaD, f);

// calculate polar ellipse coordinates
[th, rho] = mathieu_ell_in_pol(a, b, angle);
// and convert them to Cartesian
[x_elp, y_elp] = mathieu_pol2cart(th, rho);

// plotting and ...
plot(x_el(1,:), y_el(1,:), 'b--', x_elp, y_elp, 'c:', xA, yA, 'ro', xB, yB, 'go', xC, yC, 'ro', xD, yD, 'go'); xgrid;
legend('mathieu_ell2cart', ...
'mathieu_ell_in_pol -> mathieu_pol2cart',pos=4);
// ... decorating
gca().font_size = 4; xstring(xA, yA, '$A$'); xstring(xB, yB, '$B$');
xstring(xC, yC, '$C$'); xstring(xD, yD, '$D$');
xtitle('$\text{Ellipse\ with\ four\ points\ (mathieu\_ell2cart\ example)}$','$x$','$y$');
h = gca(); h.isoview = 'on'; dd=6; h.data_bounds = [-dd dd -dd dd];
h.font_size = 4; h.title.font_size=4; h.x_label.font_size=4; h.y_label.font_size=4;

// summary about ellipse and 4 points in Cartesian and elliptic coordinates
/*printf('\n\nEllipse with semiaxes: major (along x) a=%1.0f, minor (along y) b=%1.0f and linear eccentricity f=%1.0f, with xi=%f \n', a, b, f, xi(1));
printf('\tpoint A=(%f, %f): xi=%f, eta=%f\n', xA, yA, xi(1), etaA);
printf('\tpoint B=(%f, %f): xi=%f, eta=%f\n', xB, yB, xi(1), etaB);
printf('\tpoint C=(%f, %f): xi=%f, eta=%f\n', xC, yC, xi(1), etaC);
printf('\tpoint D=(%f, %f): xi=%f, eta=%f\n', xD, yD, xi(1), etaD);*/
