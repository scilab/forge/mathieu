// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
[nu,c] = mathieu_mathieuexp(2,0.3,12);
assert_checkalmostequal(nu,0.6017495,1.e-6);
e = [
   -9.88768939D-30  
    1.79783944D-26  
   -2.73202657D-23  
    3.40858498D-20  
   -3.41652567D-17  
    2.67748360D-14  
   -1.58429148D-11  
    6.75539832D-09  
   -1.94389767D-06  
    3.41692314D-04  
   -3.09110867D-02  
    9.83466428D-01  
    1.78079782D-01  
   -1.12131296D-02  
    1.75443312D-04  
   -1.26576850D-06  
    5.27482070D-09  
   -1.43342037D-11  
    2.74244936D-14  
   -3.89532618D-17  
    4.27091273D-20  
   -3.72436392D-23  
    2.64494502D-26  
   -1.55940011D-29  
    7.75504446D-33  
];
assert_checkalmostequal(c,e,1.e-6,[],"element");

// Stability curves of Mathieu equation
// (see fig. 3 of [1] for detailed view or change N to 180 manually)
N=30;
a=linspace(-2, 7, N);
q=linspace(-4, 4, N);
st=zeros(N,N);
for j=1:length(a)
    for h=1:length(q)
        st(h, j)=mathieu_mathieuexp(a(j), q(h), 12);
    end 
end
contour(q, a, imag(st), N);
xtitle('Stability curves of Mathieu equation', 'q', 'a');