// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2012 - N. O. Strelkov, NRU MPEI
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

[th, r]=mathieu_cart2pol(1, 1);
assert_checkalmostequal(th, 0.7853982, 1.e-6);
assert_checkalmostequal(r, 1.4142136, 1.e-6);

// draw a unit square
[th, r]=mathieu_cart2pol([1 -1 -1 1 1]/2,[1 1 -1 -1 1]/2);
polarplot(th, r, strf='131', style=2); xgrid;
xtitle('From Cartesian to polar coordinates (unit square)', 'x', 'y');
